/**
 *	@author			Hans Alves
 *	@version		1.0
 *	@since			0.1
 *
 *	@description	TestClass blabla
 *	more description
 *	foobar
 *
 *	quux
 */
global with sharing class TestClass implements System.Schedulable {
	/**
	 * @description This is a test constant
	 */
	public static final String test = 'FOOBAR';

	/** @description this is a counter that counts up from 0 */
	public Integer counter {
		public get {
			if (counter == null) {
				counter = 0;
			}
			return counter;
		}
		private set {
			counter = value;
		}
	}

	/**
	 * @param ctx the context passed by the scheduler
	 * @return nothing
	 */
	public void execute(System.SchedulableContext ctx) {
		System.debug(test + (counter++));
	}

	/**
	 * @param firstName the first name of the caller
	 * @param lastName the last name of the caller
	 * @return A string greeting the user
	 */
	public String hello(String firstName, String lastName) {
		return 'Hello ' + firstName + ' ' + lastName;
	}
	/**
	 * @param firstName the first name of the caller
	 * @param lastName the last name of the caller
	 * @return A string greeting the user
	 */
	@future(callout=true)
	public void futureMethod(String firstName, String lastName) {
		System.debug(hello(firstName, lastName) + ' from the future');
	}

	global List<String> genericTest(Map<Id, List<String>> param1) {
		return new List<String>{ 'foo', 'bar' };
	}

	global Map<List<Integer>, Set<Map<Id, String>>> genericTest2(Map<Id, List<String>> param1) {
		return new List<String>{ 'foo', 'bar' };
	}

	/**
	 * @description This is a list of strings
	 */
	global String[] strings;
	/**
	 * @description This is a list of maps of lists of decimals indexed by an Id
	 */
	global Map<Id, List<Decimal>>[] listofmaps;

	static Boolean isActive = true; // should not show up because no scope declared

	/**
	 * @description this will only show up if the --scopes parameter is passed and includes private
	 */
	private Decimal foo;

	/**
	 * @description the constructor does nothing
	 */
	public TestClass() {
	}

	/**
	 * @description this exception is never thrown
	 */
	private with sharing class TestException extends Exception {
	}
}
