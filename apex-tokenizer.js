"use strict";
const fs = require("fs");

const keywords = new Set([
	"abstract",
	"activate",
	"and",
	"any",
	"as",
	"asc",
	"autonomous",
	"begin",
	"break",
	"bulk",
	"by",
	"cast",
	"catch",
	"class",
	"collect",
	"commit",
	"const",
	"continue",
	"convertcurrency",
	"default",
	"delete",
	"desc",
	"do",
	"else",
	"end",
	"exit",
	"export",
	"extends",
	"final",
	"finally",
	"for",
	"from",
	"future",
	"global",
	"goto",
	"group",
	"having",
	"hint",
	"if",
	"implements",
	"import",
	"in",
	"inner",
	"insert",
	"instanceof",
	"interface",
	"into",
	"join",
	"like",
	"limit",
	"loop",
	"merge",
	"new",
	"not",
	"nulls",
	"of",
	"on",
	"or",
	"outer",
	"override",
	"package",
	"parallel",
	"pragma",
	"private",
	"protected",
	"public",
	"return",
	"returning",
	"rollback",
	"select",
	"sort",
	"stat",
	"static",
	"super",
	"switch",
	"synchronized",
	"testmethod",
	"then",
	"throw",
	"tolabel",
	"transaction",
	"try",
	"undelete",
	"update",
	"upsert",
	"using",
	"virtual",
	"webservice",
	"when",
	"where",
	"while"
]);

const values = new Set([
	"false",
	"last_90_days",
	"last_month",
	"last_n_days",
	"last_week",
	"next_90_days",
	"next_month",
	"next_n_days",
	"next_week",
	"null",
	"this",
	"this_month",
	"this_week",
	"today",
	"tomorrow",
	"true",
	"yesterday"
]);

const types = {
	decimal: "Decimal",
	double: "Double",
	exception: "Exception",
	integer: "Integer",
	list: "List",
	map: "Map",
	number: "Number",
	object: "Object",
	sobject: "SObject",
	set: "Set",
	string: "String",
	search: "Search",
	system: "System",
	trigger: "Trigger",
	type: "Type"
};

class ParseError {
	constructor(msg) {
		this.message = msg;
	}
}

class InputStream {
	constructor(input) {
		this.pos = 0;
		this.col = 0;
		this.line = 1;
		this.input = input;
	}
	peek() {
		return this.input.charAt(this.pos);
	}
	next() {
		var ch = this.input.charAt(this.pos++);
		if (ch !== "\n") this.col++;
		else {
			this.line++;
			this.col = 0;
		}
		return ch;
	}
	eof() {
		return this.peek() === "";
	}
	croak(msg) {
		throw new ParseError(msg + " (" + this.line + ":" + this.col + ")");
	}
}

class Token {
	constructor(type, value, line, col) {
		this.type = type;
		this.value = value;
		this.line = line;
		this.col = col;
	}
}

class Tokenizer {
	constructor(input) {
		this.current = null;
		if (typeof inputStream === "string") {
			this.inputStream = new InputStream(input);
		} else if (input instanceof InputStream) {
			this.inputStream = input;
		}
	}
	isWhiteSpace(ch) {
		return /^\s$/.test(ch);
	}
	isDigit(ch) {
		return /^\d$/.test(ch);
	}
	isHexDigit(ch) {
		return /^[0-9a-fA-F]$/.test(ch);
	}
	isLetter(ch) {
		return /^[a-zñáéíóúàèìòùäëïöüħðþåãøłæœ]$/i.test(ch);
	}
	isAlnum(ch) {
		return /^[a-zñáéíóúàèìòùäëïöüħðþåãøłæœ0-9_]$/i.test(ch);
	}
	isPunctuation(ch) {
		return /^[(){}\[\];,?:.@=+*\/\^<>&|%!-]$/.test(ch);
	}
	readWhile(test) {
		var s = "";
		while (test(this.inputStream.peek())) s += this.inputStream.next();
		return s;
	}
	readNumber() {
		var n = this.readWhile(this.isDigit);
		if (this.inputStream.peek() === ".") {
			n += this.inputStream.next();
			n += this.readWhile(this.isDigit);
		} /* // apex doesn't support hex values
		 else if (n == "0" && this.inputStream.peek() == "x") {
			n += this.inputStream.next();
			n += this.readWhile(this.isHexDigit);
		}*/
		return new Token("number", n, this.line, this.col);
	}
	readString() {
		var s = this.inputStream.next();
		while (!this.inputStream.eof()) {
			var c = this.inputStream.next();
			if (c == "\\") {
				c += this.inputStream.next();
			}
			s += c;
			if (c == "'" || c == "\n") break;
		}
		if (s.slice(-1) != "'") {
			console.log("String", s);
			this.inputStream.croak("Unexpected end of string");
		}
		return new Token("string", s, this.line, this.col);
	}
	readComment(c) {
		if (c == "//") c += this.readWhile((ch) => ch != "\n");
		else {
			c += this.readWhile((ch) => ch != "*");
			c += this.inputStream.next();
			while (this.inputStream.peek() != "/") {
				c += this.readWhile((ch) => ch != "*");
				c += this.inputStream.next();
			}
			c += this.inputStream.next();
		}
		return new Token("comment", c, this.line, this.col);
	}
	readWord() {
		var w = this.readWhile(this.isAlnum);
		if (keywords.has(w.toLowerCase()))
			return new Token("keyword", w, this.line, this.col);
		return new Token("identifier", w, this.line, this.col);
	}
	readWhitespace() {
		var w = this.readWhile(this.isWhiteSpace);
		return new Token("whitespace", w, this.line, this.col);
	}
	readNext() {
		if (this.inputStream.eof()) return null;
		var ch = this.inputStream.peek();
		if (this.isWhiteSpace(ch)) return this.readWhitespace();
		if (this.isDigit(ch)) return this.readNumber();
		if (this.isLetter(ch)) return this.readWord();
		if (ch == "'") return this.readString();
		if (this.isPunctuation(ch)) {
			ch = this.inputStream.next();
			var ch2 = ch + this.inputStream.peek();
			if (ch2 == "//" || ch2 == "/*") {
				ch += this.inputStream.next();
				return this.readComment(ch);
			}
			return new Token("punctuation", ch, this.line, this.col);
		}
		this.inputStream.croak("Can't handle character: `" + ch + "'");
	}
	peek() {
		return this.current || (this.current = this.readNext());
	}
	next() {
		var tok = this.current;
		this.current = null;
		return tok || this.readNext();
	}
	eof() {
		return this.peek() === null;
	}
	get line() {
		return this.inputStream.line;
	}
	get col() {
		return this.inputStream.col;
	}
}

module.exports = {
	InputStream,
	Tokenizer
};
