"use strict";

const path = require("path");
const fs = require("fs");
const parseArgs = require("minimist");
const Mustache = require("mustache");

const Koa = require("koa");
const mount = require("koa-mount");
const serve = require("koa-static");

const MarkdownIt = require('markdown-it');
const md = new MarkdownIt({
	html: true // Allow inline html in comments
});

const codescan = require("./apex-tokenizer.js");

function help() {
	console.log("Usage: node apexdoc.js [--port=PORT] [-s|--nosource] (-d DIR|--dir=DIR) [--title=TITLE] [--scopes=global,public,protected,private]");
}

let argv = {};

function getRenderData(data, path) {
	return {
		title: argv.title ? argv.title : "apexdoc",
		base: argv.base ? argv.base : "http://localhost:8080/",
		source: argv.source,
		url: path,
		data
	};
}

const scopes = new Set([
	"webservice",
	"global",
	"public",
	"protected",
	"private"
]);

class Doc {
	constructor(obj){
		Object.assign(this, obj);
	}

	get pairs(){
		let rv = []
		Object.keys(this).forEach((k) => {
			if (this.hasOwnProperty(k) && this[k] instanceof Array){
				this[k].forEach((v) => {
					rv.push({k, v});
				});
			}
		});
		return rv;
	}
}

class DocElement {
	tokens;
	attributes = [];
	classes = [];
	constructors = [];
	methods = [];
	_ = [];
	constructor(tokens, depth, classDepth) {
		this.depth = depth;
		let i = tokens.length - 2; // skip last token, it will be added later in de genDocs loop
		let startTokens = [];
		while (i >= 0) {
			let token = tokens[i--];
			if (
				((token.value === "{" || token.value === "}" || token.value === ";") &&
					token.depth === classDepth) ||
				token.depth < classDepth
			) {
				// if token.depth > classDepth we are inside the {} of a getter and setter pair
				break;
			} else if (token.depth >= classDepth) {
				startTokens.push(token);
			}
		}
		this.tokens = startTokens.reverse();
	}

	addSub(el) {
		switch (el.type) {
			case "attribute":
				this.attributes.push(el);
				break;
			case "property":
				this.attributes.push(el);
				break;
			case "class":
				this.classes.push(el);
				break;
			case "interface":
				this.classes.push(el);
				break;
			case "constructor":
				this.constructors.push(el);
				break;
			case "method":
				this.methods.push(el);
				break;
			default:
				this._.push(el);
				break;
		}
	}

	findTypeAndName() {
		let filteredTokens = this.tokens.filter(
			(tok) => tok.type !== "whitespace" && tok.type !== "comment"
		);
		let i;
		for (i = 0; i < filteredTokens.length; ++i) {
			let token = filteredTokens[i];
			if (token.value === "@" && (i + 2 < filteredTokens.length && filteredTokens[i + 2].value === "(")){
				// i + 1 should be the name of the annotation
				// if i + 2 is a ( skip it so we don't assume the name of the annotation is a function name
				for (i += 2;i < filteredTokens.length; ++i){
					token = filteredTokens[i];
					if (token.value === ")"){
						break;
					}
				}
			}
			let lcval = token.value.toLowerCase();
			if (lcval === "class" || lcval === "interface") {
				this._type = lcval;
				this._name = filteredTokens[i + 1].value;
				break;
			}
			if (lcval === "(") {
				if (i == 1 || scopes.has(filteredTokens[i - 2].value.toLowerCase())) {
					// no typename in front of the identifier
					this._type = "constructor";
				} else {
					this._type = "method";
					this._valueType = filteredTokens[i - 2].value;
					filteredTokens[i - 2].type = "typename";
				}
				this._name = filteredTokens[i - 1].value;
				filteredTokens[i - 1].type = "declname";
				break;
			}
			if (lcval === "{") {
				this._type = "property";
				this._name = filteredTokens[i - 1].value;
				filteredTokens[i - 1].type = "declname";
				if (this._name !== "get" && this._name !== "set"){
					this._valueType = filteredTokens[i - 2].value;
					filteredTokens[i - 2].type = "typename";
				}
				break;
			}
			if (lcval === "=" || lcval === ";") {
				this._type = "attribute";
				this._name = filteredTokens[i - 1].value;
				filteredTokens[i - 1].type = "declname";
				this._valueType = filteredTokens[i - 2].value;
				filteredTokens[i - 2].type = "typename";
				break;
			}
		}
		if (this._valueType === "]"){
			// It's a list, so add the [ and token in front of it
			let typeTokens = [
				filteredTokens[i - 4],
				filteredTokens[i - 3],
				filteredTokens[i - 2]
			];
			this._valueType = typeTokens.map((tok) => tok.value).join("");
		}
		if (this._valueType && this._valueType.startsWith(">")){
			// It's a generic type, go back to the matching < and start at the token in front of it
			let depth = 0;
			let maxDepth = 0;
			let typeTokens = [];
			for (i -= 2; i >= 0; --i){
				let token = filteredTokens[i];
				if (token.value === ">"){
					++depth;
				} else if (token.value === "<"){
					--depth;
				}
				maxDepth = Math.max(depth, maxDepth);
				typeTokens.push(token);
				if (depth === 0 && maxDepth > 0){
					break;
				}
			}
			if (i > 0){
				typeTokens.push(filteredTokens[i - 1]);
			}
			this._valueType = typeTokens.reverse().map((tok) => tok.value).join("");
		}
		this.attributes.sort((a, b) => (a.name > b.name ? 1 : (a.name == b.name ? 0 : -1)));
		this.constructors.sort((a, b) => (a.name > b.name ? 1 : (a.name == b.name ? 0 : -1)));
		this.methods.sort((a, b) => (a.name > b.name ? 1 : (a.name == b.name ? 0 : -1)));
		this.classes.sort((a, b) => (a.name > b.name ? 1 : (a.name == b.name ? 0 : -1)));
	}

	get type() {
		// class or interface
		// method
		// constructor
		// attribute or property
		if (!this._type) {
			this.findTypeAndName();
		}
		return this._type;
	}

	get name() {
		if (!this._name) {
			this.findTypeAndName();
		}
		return this._name;
	}

	get valueType() {
		if (!this._valueType) {
			this.findTypeAndName();
		}
		return this._valueType;
	}

	findDeclaration(){
		let delim;
		switch (this.type){
			case "attribute":
				delim = ";"
				break;
			case "property":
			case "class":
			case "interface":
				delim = "{"
				break;
			case "constructor":
			case "method":
				delim = ")";
				break;
			default:
				break;
		}
		let toks = [];
		let started = false;
		let scopeSeen = false;
		for (let idx = 0; idx < this.tokens.length; ++idx){
			let tok = this.tokens[idx];
			if (!started && tok.type !== "whitespace" && tok.type !== "comment"){
				started = true;
			}
			let lcval = tok.value.toLowerCase();
			if (scopes.has(lcval)){
				scopeSeen = true;
				this._scope = lcval;
			}
			if (scopeSeen && tok.value === delim){
				if (delim !== "{"){
					toks.push(tok);
				}
				break;
			}
			if (started){
				toks.push(tok);
			}

		}
		this._declaration = toks.map((tok) => tok.value).join("");
	}

	get declaration(){
		if (!this._declaration){
			this.findDeclaration();
		}
		return this._declaration;
	}

	get scope(){
		if (!this._scope){
			this.findDeclaration();
		}
		return this._scope;
	}

	_docComment = null;
	get docComment(){
		if (this._docComment === null){
			this._docComment = "";
			let token = this.tokens.find((tok) => tok.type === "comment" && tok.value.startsWith("/**"));
			if (token){
				this._docComment = token.value
			}
		}
		return this._docComment;
	}

	processDocComment(){
		let comment = this.docComment;
		let lines = comment.replace(/(\/\*\*)|(\s+\*\/?)/g, "\n").trim().split("\n");
		let rv = {};
		let key = null;
		lines.forEach((line) => {
			let m = line.match(/\@(\w+)\s+(.*)/)
			if (m){
				key = m[1];
				if (key in rv){
					rv[key].push(m[2]);
				} else {
					rv[key] = [m[2]];
				}
			} else if (key) {
				let arr = rv[key];
				arr[arr.length - 1] += "\n" + line;
			}
		});
		Object.keys(rv).forEach((key) => {
			rv[key].forEach((value, index, array) => {
				array[index] = md.render(value);
			});
		});
		return rv;
	}

	_doc = null;
	get doc(){
		if (this._doc === null){
			this._doc = new Doc(this.processDocComment());
		}
		return this._doc;
	}
}

async function genDocs(fn) {
	let rv = null;
	let fullname = path.join(
		argv.dir,
		fn.replace(new RegExp("^" + argv["path-prefix"]), "")
	);
	if (!fs.existsSync(fullname)){
		return null;
	}
	let content = await fs.promises.readFile(fullname, {encoding: "utf-8"});
	let tokenizer = new codescan.Tokenizer(new codescan.InputStream(content));
	let tokens = [];
	let depth = 0;
	let decls = [];
	let idx = 0;
	while (!tokenizer.eof()) {
		let token = tokenizer.next();
		tokens.push(token);
		if (token.value === "{") {
			++depth;
		}
		if (token.value === "}") {
			--depth;
		}
		token.idx = idx++;
		token.depth = depth;
		if (token.type === "keyword" && scopes.has(token.value.toLowerCase())) {
			// next should be type and name
			decls.push(new DocElement(tokens, depth, decls.length));
		}
		decls.forEach((decl) => {
			decl.tokens.push(token);
		});
		if (decls.length > 0) {
			let topdecl = decls[decls.length - 1];
			if (topdecl.depth === depth) {
				if (token.value === ";" || token.value === "}") {
					// end of declaration
					let decl = decls.pop();
					if (decls.length > 0) {
						decls[decls.length - 1].addSub(decl);
					} else {
						rv = decl;
					}
					decl.processDocComment();
					decl.findTypeAndName();
					decl.findDeclaration();
					decl.attributes = decl.attributes.filter((itm) => argv.scopes.indexOf(itm.scope) != -1);
					decl.methods = decl.methods.filter((itm) => argv.scopes.indexOf(itm.scope) != -1);
					decl.constructors = decl.constructors.filter((itm) => argv.scopes.indexOf(itm.scope) != -1);
					decl.classes = decl.classes.filter((itm) => argv.scopes.indexOf(itm.scope) != -1);
				}
			}
		}
	}
	rv.filename = fn;
	return rv;
}

async function findFiles(dir, dirpath) {
	let files = await fs.promises.readdir(dir, {withFileTypes: true});
	let classes = [];
	let subdirs = [];
	files.forEach((dirent) => {
		if (dirent.isFile() && dirent.name.match(/\.cls$/)) {
			classes.push(path.join(dirpath, dirent.name));
		} else if (dirent.isDirectory() && !dirent.name.startsWith(".")) {
			subdirs.push(
				findFiles(path.join(dir, dirent.name), path.join(dirpath, dirent.name))
			);
		}
	});
	return classes.concat(...(await Promise.all(subdirs)));
}

const layout = path.join(__dirname, "views", "layout.hbs");
async function render(file, data) {
	let tpl = await fs.promises.readFile(
		path.join(__dirname, "views", file + ".hbs"),
		{encoding: "utf-8"}
	);
	let body = Mustache.render(tpl, data);
	tpl = await fs.promises.readFile(layout, {encoding: "utf-8"});
	return Mustache.render(tpl, Object.assign({body}, data));
}

async function index(ctx) {
	if (ctx.method !== "GET") {
		ctx.response.status = 405;
		return;
	}
	if (
		ctx.request.path.match(/^\/static/) ||
		ctx.request.path.match(/favicon.ico$/)
	) {
		return;
	}
	let file = ctx.request.path.replace(/(^\/)|(\.html$)/g, "");
	if (file) {
		let docs = await genDocs(file);
		if (docs){
			ctx.response.body = await render("doc", getRenderData(docs, `${file}.html`));
		} else {
			ctx.response.status = 404;
		}
	} else {
		let files = await findFiles(argv.dir, argv["path-prefix"]);
		files.sort();
		ctx.response.body = await render(
			"index",
			getRenderData(files, ctx.request.path)
		);
	}
}

function main() {
	argv = parseArgs(process.argv.slice(2), {
		string: ["d", "dir"],
		boolean: ["h", "help"],
		boolean: ["s", "nosource"],
		default: {"path-prefix": null, "scopes": "global,public"}
	});
	if (argv.h || argv.help) {
		return help();
	}
	argv.source = !(argv.s || argv.nosource);

	if (!argv.dir && argv.d) {
		argv.dir = argv.d;
	}
	if (!argv.dir) {
		console.error("No directory provided");
		help();
		process.exit(1);
	}
	if (argv["path-prefix"] === null) {
		argv["path-prefix"] = path.basename(argv.dir);
	}
	if (argv.scopes.indexOf("global") != -1){
		argv.scopes += ",webservice";
	}
	const app = new Koa();

	app.use(mount("/static", serve(path.join(__dirname, "static"))));
	app.use(index);

	let port = argv.port || process.env.PORT || 8080;
	app.listen(port);
	console.log("listening at port", port);
}

main();
