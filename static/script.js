"use strict";

window.addEventListener("load", function init(){
	function togglePopover(evt){
		let target = evt.currentTarget;
		while (target && !target.classList.contains("dox-popover")){
			target = target.parentNode;
		}
		if (target){
			target.dataset.pop = !(target.dataset.pop === "true");
		}
	}

	[...document.getElementsByClassName("dox-pop-btn")].forEach((el) => {
		el.addEventListener("click", togglePopover);
	});
});
