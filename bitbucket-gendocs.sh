#/bin/bash

# FOR USE BY BITBUCKET PIPELINE, ASSUMES IT RUNS IN redbookict/sfdx
set -x

cd /apexdox/
node apexdox.js --dir=$BITBUCKET_CLONE_DIR/force-app/ --tile=$TITLE --scopes=global,public &
sleep 1
cd /tmp/
wget -P $DIR_NAME -nv -nH -r -E localhost:8080
cd $DIR_NAME
find . -type f -print0 | xargs -0 sed 's|<base href="http://localhost:8080/">|<base href="https://'"$DOCS_SERVER/docs/$DIR_NAME"'/">|' -i
cd ..

#==Set ssh key and upload the docs to willikins
openssl enc -d -aes-256-cbc -pbkdf2 -in ~/.ssh/id_ed25519.enc -out ~/.ssh/id_ed25519 -pass 'env:PASSWD'
chmod 600 ~/.ssh/id_ed25519;
scp -P $SSH_PORT -r $DIR_NAME bitbucket@$DOCS_SERVER:/var/www/docs/
