# ApexDox

An apex documentation server that can take code comments in a javadoc like format and generate documentation pages on the fly.

## Installation

1. Clone this repository with `git clone git@bitbucket.org:RedbookTeam/apexdox.git`
1. Go to the created directory `cd apexdox`
1. Install the javascript dependencies with `npm install`

## Usage

The server should be started with a directory that contains apex classes as a parameter. E.g.

    $ node apexdox.js --dir=/path/to/my/sfdx/project/force-app --tile=MyProject --scopes=global,public

Because nodejs needs to be able to find the javascript dependencies it is easiest to run this server from its installation directory and provide the full path to the directory with your apex project to the `dir` parameter.

It will search through the given directory for `*.cls` files. For these files it can generate a documentation page.

The documentation page will only include elements with an explicit scope, if that scope is included in the `--scopes` parameter or `global` and `public` if the parameter not is given.

The generator wil search for a doc-comment above each declaration. To be recognized the doc comment should start with `/**`.

The doc-comment should contain declarations of the form `@somename value` the value can span multiple lines. These declarations will be included in a table in the documentation.

To generate all documentation in a directory wget can be used after starting the server as follows

    $ wget -P build -nv -nH -r -E localhost:8080

This will generate all documentation and store it in a directory called build.

Because the resulting files will probably be hosted on a server and might not in the root folder you can change the base url in all files with the following commands

    $ cd build
    $ find . -type f -print0 | xargs -0 sed 's|<base href="http://localhost:8080/">|<base href="https://wow.redbookict.nl/somefolder/">|' -i

## TODO

- Better styling and layout
- More testing
